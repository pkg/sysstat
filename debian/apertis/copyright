Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1999-2023, Sebastien GODARD (sysstat (at) orange (dot) fr)
License: GPL-2

Files: man/* nls/*
Copyright: © 1998-2024, Sebastien Godard (sysstat <at> orange.fr)
License: GPL-2+

Files: contrib/isag/*
Copyright: © 2000, 2001 David Doubrava (linux_monitor(at)volny(dot)cz)
License: GPL-2+

Files: activity.c
 common.c
 count.c
 format.c
 iostat.c
 json_stats.c
 pcp_def_metrics.c
 pcp_stats.c
 pidstat.c
 pr_stats.c
 pr_xstats.c
 raw_stats.c
 rd_sensors.c
 rd_stats.c
 rndr_stats.c
 sa_common.c
 sa_conv.c
 sa_wrap.c
 sadc.c
 sadf.c
 sadf_misc.c
 sar.c
 svg_stats.c
 systest.c
 xml_stats.c
Copyright: 1998-2023, Sebastien GODARD (sysstat <at> orange.fr)
License: GPL-2+

Files: cifsiostat.c
Copyright: 2010, Red Hat, Inc.
License: GPL-2+

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: contrib/irqstat/*
Copyright: 2015, Lance W. Shelton
License: Expat

Files: contrib/irqtop/*
Copyright: no-info-found
License: GPL-2+

Files: contrib/isag/isag
Copyright: 2000, 2001, David Doubrava (linux_monitor(at)volny(dot)cz)
License: GPL-2+

Files: debian/*
Copyright: 2000-2024, Robert Luberda <robert@debian.org>
License: GPL-2+

Files: debian/patches/01-isag.patch
Copyright: 2000, 2001, David Doubrava <linux_monitor@volny.cz>.
License: GPL-2+

Files: debian/patches/14-simtest-run-all.patch
Copyright: 1998-2023, Sebastien GODARD (sysstat <at> orange.fr)
License: GPL-2+

Files: debian/patches/15-sa2-bash.patch
Copyright: 1999-2023, Sebastien Godard (sysstat <at> orange.fr)
License: GPL-2+

Files: ioconf.c
Copyright: 2004, Red Hat (Charlie Bennett <ccb@redhat.com>)
License: GPL-2+

Files: mpstat.c
Copyright: 2022, Oracle and/or its affiliates.
 2000-2023, Sebastien GODARD (sysstat <at> orange.fr)
License: GPL-2+

Files: sysstat-12.7.5.spec
Copyright: no-info-found
License: GPL

Files: tapestat.c
Copyright: 2015, Hewlett-Packard Development Company, L.P.
License: GPL-2+

Files: tests/12.0.1/*
Copyright: 1998-2023, Sebastien GODARD (sysstat <at> orange.fr)
License: GPL-2+

Files: tests/12.0.1/ioconf.c
Copyright: 2004, Red Hat (Charlie Bennett <ccb@redhat.com>)
License: GPL-2+

Files: tests/create_data-extra.c
 tests/create_data-ukwn.c
Copyright: 1998-2023, Sebastien GODARD (sysstat <at> orange.fr)
License: GPL-2+

Files: CHANGES INSTALL Makefile.in cifsiostat.h common.h configure.ac count.h cron/sysstat-collect.service.in cron/sysstat-collect.timer.in cron/sysstat-rotate.service.in cron/sysstat-rotate.timer.in cron/sysstat-summary.service.in cron/sysstat-summary.timer.in cron/sysstat.sleep.in do_test iconfig ioconf.h iostat.h json_stats.h man/tapestat.1 mpstat.h nls/af.po nls/be.po nls/cs.po nls/da.po nls/de.po nls/eo.po nls/es.po nls/eu.po nls/fi.po nls/fr.po nls/fur.po nls/gl.po nls/hr.po nls/hu.po nls/id.po nls/it.po nls/ja.po nls/ka.po nls/ko.po nls/ky.po nls/lv.po nls/nb.po nls/nn.po nls/pl.po nls/pt.po nls/pt_BR.po nls/ro.po nls/ru.po nls/sk.po nls/sr.po nls/sv.po nls/tr.po nls/uk.po nls/vi.po nls/zh_CN.po nls/zh_TW.po pcp_def_metrics.h pcp_stats.h pidstat.h pr_stats.h pr_xstats.h raw_stats.h rd_sensors.h rd_stats.h rndr_stats.h sa.h sa1.in sa2.in sa_conv.h sadf.h svg_stats.h sysconfig.in sysstat.in sysstat.ioconf sysstat.service.in systest.h tapestat.h tests/12.0.1/common.h tests/12.0.1/inisysconfig.h tests/12.0.1/iniversion.h tests/12.0.1/ioconf.h tests/12.0.1/pr_stats.h tests/12.0.1/rd_sensors.h tests/12.0.1/rd_stats.h tests/12.0.1/sa.h tests/data-10.3.1 tests/data-9.1.5 tests/data-extra-12.1.7 tests/data-ppc-11.7.2 tests/data-ukwn tests/data-ukwn0 tests/expected.cifsiostat-V-env tests/expected.iostat-V-env tests/expected.mpstat-V-env tests/expected.pidstat-V-env tests/expected.sadc-V-env tests/expected.sadf-V-env tests/expected.sar-V-env tests/expected.tapestat-V-env version.in xml_stats.h
Copyright: © 1998-2024, Sebastien Godard (sysstat <at> orange.fr)
License: GPL-2+
